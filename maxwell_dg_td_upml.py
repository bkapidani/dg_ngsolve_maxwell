from ngsolve.comp import *
from ngsolve import *
from math import pi

from netgen.geom2d import SplineGeometry
geo = SplineGeometry()
geo.AddRectangle( (0, 0), (1, 1), bcs = ("bottom", "disc", "top", "left"))
geo.AddRectangle( (1, 0), (1.25, 1), bcs = ("bottom_upml", "right", "top_pml", "disc"))
mesh1 = Mesh( geo.GenerateMesh(maxh=0.05))

#~ eps0 = 8.8541878176e-12
#~ mu0  = 4*math.pi*1e-7
eps0 = 0.5
mu0  = 1
epsr = 1
mur  = 1

#~ sigma = CoefficientFunction( 1 if >1 else 0)
sigma = CoefficientFunction( IfPos(x-1,0,0))
#~ Null_scalar = CoefficientFunction(0)

# For the time stepping
tau = 1e-3
tend = 1.5
t = 0

#~ mesh1 = Mesh(unit_square.GenerateMesh(maxh=0.05))

k = 5 # lowest polynomial order

#~ fes_h = VectorL2(mesh1, order=k+1)
fes_hx = L2(mesh1, order=k+1)
fes_hy = L2(mesh1, order=k+1)
fes_e  = L2(mesh1, order=k)

fes = FESpace( [fes_hx,fes_xy,fes_e] )

u_hx,u_hy,u_ez = fes.TrialFunction()
v_hx,v_hy,v_ez = fes.TestFunction()

matgrad_hx = grad(v_hx)
matgrad_hy = grad(v_hy)
curl_z_h    = CoefficientFunction( matgrad_hx[1]-matgrad_hy[0] )

matgrad_Hx = grad(u_hx)
matgrad_Hy = grad(u_hy)
curl_z_H    = CoefficientFunction( matgrad_Hx[1]-matgrad_Hy[0] )

paramt = Parameter(0)
print (mesh1.GetBoundaries())
Hin = CoefficientFunction( [sin(2*pi*6*paramt)*sin(pi*y) if bc=="left" else None for bc in mesh1.GetBoundaries()] )

n = specialcf.normal(2) # 2 is the space dimension
tang = CoefficientFunction( (-n[1],n[0]) )

a1  = BilinearForm(fes)
a1 += SymbolicBFI ( -u_ez*curl_z_h )
a1 += SymbolicBFI ( -0.5*(u_ez + u_ez.Other())*((CoefficientFunction( (v_hx,v_hy) )-CoefficientFunction( (v_hx.Other(),v_hy.Other()) ))*tang), VOL, skeleton=True)
a1 += SymbolicBFI ( -1*(u_ez*(CoefficientFunction( (v_hx,v_hy) )*tang)),  BND, skeleton=True)

a2  = BilinearForm(fes)
a2 += SymbolicBFI (  v_ez*curl_z_H )
a2 += SymbolicBFI (  0.5*(v_ez + v_ez.Other())*((CoefficientFunction( (u_hx,u_hy) )-CoefficientFunction( (u_hx.Other(),u_hy.Other()) ))*tang), VOL, skeleton=True)
a2 += SymbolicBFI (  1*(v_ez*(CoefficientFunction( (u_hx,u_hy) )*tang)), BND, skeleton=True)

# soft source incoming from "left" boundary side
a2 += SymbolicBFI (  0.5*(v_ez*(CoefficientFunction( (0,u_hy.Other(bnd=Hin)) )*tang) ), BND, skeleton=True)

a3  = BilinearForm(fes)
a3 += SymbolicBFI (  u_hy*(mur*mu0*v_hy) )
a3 += SymbolicBFI ( -u_hy*(0.5*tau*sigma*v_hy) )
 
# set initial conditions
#~ u0 = CoefficientFunction( (exp ( -400 * ( (x-0.5)*(x-0.5) + (y-0.5)*(y-0.5) )),0) )
#~ u0 = CoefficientFunction( (0,0) )
#~ u1 = CoefficientFunction(0)
#~ u1 = CoefficientFunction(exp ( -400 * ( (x-0.5)*(x-0.5) + (y-0.5)*(y-0.5) )) )

while True:
	u = GridFunction(fes)

	#~ with TaskManager():
			#~ u.components[0].Set(u0)
			#~ u.components[1].Set(u1)

	Draw(u.components[0], mesh1, "mag_field")
	Draw(u.components[1], mesh1, "ele_field")



	input ("<press enter>")

	hxdofs = fes.Range(0)
	hydofs = fes.Range(1)
	edofs = fes.Range(2)
	w  = u.vec.CreateVector()
	w2 = u.vec.CreateVector()
	n= 0
	
	with TaskManager():
		while t < tend:
			paramt.Set(t)
			a1.Apply (u.vec, w)
			a3.Apply (u.vec, w2)
			fes_hx.SolveM (rho=CoefficientFunction(mur*mu0), vec=w[hxdofs])
			fes_hy.SolveM (rho=CoefficientFunction(mur*mu0)+0.5*tau*sigma, vec=w[hydofs])
			u.vec.data = w2 + tau * w
			
			a2.Apply (u.vec, w)
			fes_e.SolveM(rho=CoefficientFunction(epsr*eps0), vec=w[edofs])
			u.vec.data += tau * w

			t += tau
			if n%10 == 0:
				Redraw()
			n = n+1
			
