from ngsolve.comp import *
from ngsolve import *
from math import pi
from netgen.geom2d import unit_square

#~ eps0 = 8.8541878176e-12
#~ mu0  = 4*math.pi*1e-7
eps0 = 0.5
mu0  = 1
epsr = 1
mur  = 1

mesh1 = Mesh(unit_square.GenerateMesh(maxh=0.05))

k = 5 # lowest polynomial order

fes_h = VectorL2(mesh1, order=k)
fes_e = L2(mesh1, order=k+1)

fes = FESpace( [fes_h,fes_e] )

u_h,u_ez = fes.TrialFunction()
v_h,v_ez = fes.TestFunction()

matgrad_h = grad(v_h)
curl_h    = CoefficientFunction( matgrad_h[0,1]-matgrad_h[1,0] )

matgrad_H = grad(u_h)
curl_H    = CoefficientFunction( matgrad_H[0,1]-matgrad_H[1,0] )

paramt = Parameter(0)
print (mesh1.GetBoundaries())
Hin = CoefficientFunction( (0,[sin(2*pi*6*paramt)*sin(pi*y) if bc=="left" else None for bc in mesh1.GetBoundaries()]) )

n = specialcf.normal(2) # 2 is the space dimension
tang = CoefficientFunction( (-n[1],n[0]) )

a1  = BilinearForm(fes)
a1 += SymbolicBFI ( -u_ez*curl_h )
a1 += SymbolicBFI ( -0.5*(u_ez + u_ez.Other())*((v_h-v_h.Other())*tang), VOL, skeleton=True)
a1 += SymbolicBFI ( -1*(u_ez*(v_h*tang)),  BND, skeleton=True)

a2  = BilinearForm(fes)
a2 += SymbolicBFI (  v_ez*curl_H )
a2 += SymbolicBFI (  0.5*(v_ez + v_ez.Other())*((u_h-u_h.Other())*tang), VOL, skeleton=True)
a2 += SymbolicBFI (  1*(v_ez*(u_h*tang)), BND, skeleton=True)

a2 += SymbolicBFI (  0.5*(v_ez*(u_h.Other(bnd=Hin)*tang)), BND, skeleton=True) # soft source
 
# set initial conditions
#~ u0 = CoefficientFunction( (exp ( -400 * ( (x-0.5)*(x-0.5) + (y-0.5)*(y-0.5) )),0) )
#~ u0 = CoefficientFunction( (0,0) )
#~ u1 = CoefficientFunction(0)
#~ u1 = CoefficientFunction(exp ( -400 * ( (x-0.5)*(x-0.5) + (y-0.5)*(y-0.5) )) )

while True:
	u = GridFunction(fes)

	#~ with TaskManager():
			#~ u.components[0].Set(u0)
			#~ u.components[1].Set(u1)

	Draw(u.components[0], mesh1, "mag_field")
	Draw(u.components[1], mesh1, "ele_field")


	# For the time stepping
	tau = 1e-3
	tend = 1.5
	t = 0

	input ("<press enter>")

	hdofs = fes.Range(0)
	edofs = fes.Range(1)
	w = u.vec.CreateVector()
	n= 0
	
	with TaskManager():
		while t < tend:
			paramt.Set(t)
			a1.Apply (u.vec, w)
			fes_h.SolveM (rho=CoefficientFunction(mur*mu0), vec=w[hdofs])
			u.vec.data += tau * w
			
			a2.Apply (u.vec, w)
			fes_e.SolveM(rho=CoefficientFunction(epsr*eps0), vec=w[edofs])
			u.vec.data += tau * w

			t += tau
			if n%10 == 0:
				Redraw()
			n = n+1
			
