from ngsolve.comp import *
from ngsolve import *
from netgen.geom2d import unit_square

#~ eps0 = 8.8541878176e-12
#~ mu0  = 4*math.pi*1e-7
eps0 = 1
mu0  = 1
epsr = 1
mur  = 1

mesh1 = Mesh(unit_square.GenerateMesh(maxh=0.05))

k = 3 # lowest polynomial order

fes_e = VectorL2(mesh1, order=k+1, dirichlet=[1,2,3,4])
fes_h = L2(mesh1, order=k) # normal component boudary condition is naturally satisfied

fes = FESpace( [fes_e,fes_h] )

E,H = fes.TrialFunction()
e,h = fes.TestFunction()

matgrad_e = grad(e)
curl_e    = matgrad_e[0,1]-matgrad_e[1,0]

matgrad_E = grad(E)
curl_E    = matgrad_E[0,1]-matgrad_E[1,0]

paramt = Parameter(0)
print (mesh1.GetBoundaries())
# Hin = CoefficientFunction( [sin(30*paramt), 0, 0, 0] )
Hin = CoefficientFunction( [sin(30*paramt) if bc=="bottom" else None for bc in mesh1.GetBoundaries()] )

n = specialcf.normal(2) # 2 is the space dimension
tang = CoefficientFunction( (-n[1],n[0]) )
a1  = BilinearForm(fes)
a1 += SymbolicBFI ( H*curl_e )
a1 += SymbolicBFI ( 0.5 * (H + H.Other()) * (tang*(e-e.Other())), VOL, skeleton=True)
a1 += SymbolicBFI ( 1 * (H+H.Other(bnd=Hin)) * (tang*e), BND, skeleton=True)

a2  = BilinearForm(fes)
a2 += SymbolicBFI ( -curl_E * h )
a2 += SymbolicBFI ( -0.5 * (h+h.Other()) * (tang*(E-E.Other())), VOL, skeleton=True)
a2 += SymbolicBFI ( -1 * h * (tang*(E)), BND, skeleton=True)
a2 += SymbolicBFI ( -H*h, BND, skeleton=True)

# set initial conditions
#~ u0 = CoefficientFunction( (exp ( -400 * ( (x-0.5)*(x-0.5) + (y-0.5)*(y-0.5) )),0) )
u0 = CoefficientFunction((0,0))
u1 = CoefficientFunction(exp ( -400 * ( (x-0.5)*(x-0.5) + (y-0.5)*(y-0.5) )))
u = GridFunction(fes)
w = u.vec.CreateVector()

# with TaskManager():
# u.components[0].Set(u0)
# u.components[1].Set(u1) # shouldn´t it satisfy maxwell

Draw(u.components[0], mesh1, "ele_field")
Draw(u.components[1], mesh1, "mag_field")

# For the time stepping
tau = 1e-3
tend = 0.7

t = 0
nd = fes_h.ndof

input ("<press enter>")

edofs = fes.Range(0)
hdofs = fes.Range(1)

with TaskManager(pajetrace=100*1000*1000):
    while t < tend:
        paramt.Set(t)
        a2.Apply (u.vec, w)
        fes_h.SolveM (vec=w[hdofs])
        u.vec.data += tau * w

        a1.Apply (u.vec, w)
        fes_e.SolveM (rho=CoefficientFunction(epsr*eps0), vec=w[edofs])
        u.vec.data += tau * w

        t += tau
        Redraw(blocking=True)




