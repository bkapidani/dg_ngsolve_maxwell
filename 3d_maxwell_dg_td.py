#~ from ngsolve.comp import *
from ngsolve import *
from netgen.csg import *
import netgen.gui
from math import pi

#~ eps0 = 8.8541878176e-12
#~ mu0  = 4*pi*1e-7
eps0 = 1
mu0  = 1
epsr = 1
mur  = 1
k = 4 # lowest polynomial order
paramt = Parameter(0) # for time dependent sources

SetHeapSize(10000000)

# Geometry and mesh
cube = OrthoBrick( Pnt(-1,-1,0), Pnt(1,1,0.5) )
inf_cyl = Cylinder ( Pnt(0, 0, 0), Pnt(0, 0, 1), 1)
geo = CSGeometry()
geo.Add (cube*inf_cyl)
mesh1 = Mesh(geo.GenerateMesh(maxh=0.2)) # never forget to make an ngsolve mesh
mesh1.Curve(k) # curvature of elements

# Finite element spaces needed
fes_h = VectorL2(mesh1, order=k)
fes_e = VectorL2(mesh1, order=k+1)
fes = FESpace( [fes_h,fes_e])
u_h,u_e = fes.TrialFunction()
v_h,v_e = fes.TestFunction()

# Manually define curls of vector L2 space trial and test functions
matgrad_E = grad(u_e)
curl_E = CoefficientFunction( (matgrad_E[2,1]-matgrad_E[1,2],matgrad_E[0,2]-matgrad_E[2,0],matgrad_E[1,0]-matgrad_E[0,1]) )
matgrad_e = grad(v_e)
curl_e = CoefficientFunction( (matgrad_e[2,1]-matgrad_e[1,2],matgrad_e[0,2]-matgrad_e[2,0],matgrad_e[1,0]-matgrad_e[0,1]) )

n = specialcf.normal(3) # 3 is the space dimension

# Define coeff. fun. for initial conditions
u0 = CoefficientFunction( (0,0,0) )
u1 = CoefficientFunction( (0,exp ( -400 * ( (x*x) + (y*y) + (z-0.25)*(z-0.25)) ),0) )

# Assemble right hand-side bilinear form
a1  = BilinearForm(fes)
a1 += SymbolicBFI (   -v_h*curl_E )
a1 += SymbolicBFI (   0.5*(u_e + u_e.Other())*(Cross(v_h-v_h.Other(),n)), VOL, skeleton=True)
a1 += SymbolicBFI (   1*(u_e*Cross(v_h,n)), BND, skeleton=True)
#~ a1 += SymbolicBFI (  0.5*(u_e*Cross(v_h.Other(bnd=u0),n)), BND, skeleton=True) # soft source

a2  = BilinearForm(fes)
a2 += SymbolicBFI (   u_h*curl_e )
a2 += SymbolicBFI (  -0.5*(v_e + v_e.Other())*(Cross(u_h-u_h.Other(),n)), VOL, skeleton=True)
a2 += SymbolicBFI (  -1*(v_e*Cross(u_h,n)),  BND, skeleton=True)

u = GridFunction(fes)
while True:

	with TaskManager():
			u.components[0].Set(u0)
			u.components[1].Set(u1)

	Draw(u.components[0], mesh1, "mag_field", sd=3, draw_surf=False)
	Draw(u.components[1], mesh1, "ele_field", sd=3, draw_surf=False)

	# For the time stepping
	tau  = 1e-3
	tend = 1
	t = 0

	input ("<press enter>")

	hdofs = fes.Range(0)
	edofs = fes.Range(1)
	w = u.vec.CreateVector()
	n = 0
	
	with TaskManager():
		while t < tend:
			paramt.Set(t)
			a1.Apply (u.vec, w)
			fes_h.SolveM (rho=CoefficientFunction(mur*mu0), vec=w[hdofs])
			u.vec.data += tau * w
			
			a2.Apply (u.vec, w)
			fes_e.SolveM(rho=CoefficientFunction(epsr*eps0), vec=w[edofs])
			u.vec.data += tau * w

			if n%10 == 0:
				Redraw()
			t += tau
			n = n+1
			
